import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';

import { Album, List } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-albums-details',
  templateUrl: './albums-details.component.html',
  styleUrls: ['./albums-details.component.scss'],
  animations: [
    trigger('details', [
      // Clé de l'animation
      state(
        // Etat 1
        'open',
        style({
          backgroundColor: 'white',
        })
      ),
      state(
        // Etat 1
        'opening',
        style({
          backgroundColor: 'lightgrey',
        })
      ),
      transition('opening => open', [animate('.25s')]), // Transition et durée
    ]),
  ],
})
export class AlbumsDetailsComponent implements OnInit {
  // Classe Input permet de récupérer les data de l'enfant
  // album est liée à une entrée [album] du parent dans le sélecteur
  @Input() album: Album;

  // Propriété émitrice permettant d'envoyer des données au parent.
  @Output() onPlay: EventEmitter<Album> = new EventEmitter();
  @Output() onHide: EventEmitter<Album> = new EventEmitter();

  list: List;
  isHidden: boolean = false;
  isOpen: boolean = false;

  constructor(private AService: AlbumService) {
    this._transition();
  }

  ngOnChanges() {
    // console.log('ngOnChanges'); // S'execute à chaque changement d 'input
    if (this.album) {
      this.isHidden = false;
      this.list = this.AService.getAlbumList(this.album.id);
    }
  }

  ngDoCheck() {}

  ngOnInit(): void {}

  onListClick(trackNumber: number) {
    console.log(`Clicked on ${trackNumber}`);
  }

  onPlayClick(album: Album): void {
    this.onPlay.emit(album); // emet un album vers le parent.
  }

  onHideClick(album: Album): void {
    this.onHide.emit(album); // emet un album vers le parent.
    this.isHidden = true;
  }

  onShuffleClick(album: Album): void {
    this.list = this.AService.shuffleAlbumList(album.id);
    this.isOpen = false;
    this._transition();
    console.log(this.list);
  }

  private _transition() {
    setTimeout(() => {
      this.isOpen = true;
    }, 100);
  }
}
