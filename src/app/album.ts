export class Album {
  id: string;
  ref: string;
  name: string;
  title: string;
  description: string;
  duration: number;
  status: Status;
  url?: string;
  like?: string;
  tags?: string[];
  notes: number[];
}

export class List {
  id: string;
  list: string[];
}

export enum Status {
  On = 'on',
  Off = 'off'
}

// Type fonctionnel pour l'ordre des albums
export type Order = (a: Album, b: Album) => number;
