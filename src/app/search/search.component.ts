import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Album } from '../album';
import { NgForm } from '@angular/forms'; // template-driven
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() onSearch: EventEmitter<Album[]> = new EventEmitter();

  word: string;

  constructor(private AService: AlbumService) {}

  ngOnInit(): void {}

  onSubmit(form: NgForm): void {
    // récupération des données du formulaire
    // console.log(form);
    // console.log(form.value['word']); // récupération d'une valeur spécifique
    const search = this.AService.search(form.value['word']); // Envoi au service
    if (search) {
      this.onSearch.emit(search); // Envoi au parent
    }
  }

  onChangeEmit() {
    // $e == this.word
    const search = this.AService.search(this.word); // Envoi au service
    if (search) {
      this.onSearch.emit(search); // Envoi au parent
    }
  }
}
