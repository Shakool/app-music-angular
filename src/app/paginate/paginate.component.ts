import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AlbumService } from '../album.service';
import { Album } from '../album';

@Component({
  selector: 'app-paginate',
  templateUrl: './paginate.component.html',
  styleUrls: ['./paginate.component.scss']
})
export class PaginateComponent implements OnInit {
  @Input() CustomItemPerPage: number;
  @Output() onPageChange: EventEmitter<Album[]> = new EventEmitter();

  albums: Album[];
  currentPage: number = 1;
  itemsPerPage: number = 3;
  itemsTotal: number;
  pagesTotal: number;
  range: 1; // not used

  constructor(private AService: AlbumService) {}

  ngOnInit(): void {
    this.itemsPerPage = this.CustomItemPerPage
      ? this.CustomItemPerPage
      : this.itemsPerPage;
    this.albums = this.AService.paginate(
      (this.currentPage - 1) * this.itemsPerPage, // 0
      (this.currentPage - 1) * this.itemsPerPage + this.itemsPerPage // 3
    );
    this.itemsTotal = this.AService.getAlbumCount();
    this.pagesTotal = Math.ceil(this.itemsTotal / this.itemsPerPage);
    // Subscription à la synchro des composants de pagination
    this.AService.sendCurrentPage.subscribe(numberPage => {
      this.currentPage = numberPage; // Affectation de la page reçue à this.currentPage
    });
    this.goToPage(1);
  }

  ngOnChanges(): void {
    console.log('PageNgOnChange');
  }

  previous(): void {
    this.currentPage = this.currentPage != 1 ? this.currentPage - 1 : 1;
    this.goToPage(this.currentPage);
  }

  next(): void {
    this.currentPage =
      this.currentPage + 1 > this.pagesTotal
        ? this.pagesTotal
        : this.currentPage + 1;
    this.goToPage(this.currentPage);
  }

  goToPage(nPage: number): void {
    this.albums = this.AService.paginate(
      (nPage - 1) * this.itemsPerPage,
      (nPage - 1) * this.itemsPerPage + this.itemsPerPage
    );
    this.currentPage = nPage;
    this.onPageChange.emit(this.albums);
    this.AService.currentPage(this.currentPage); // Call de la méthode pour synchroniser les autres composants de pagination
  }

  getPagesAsArray(): Array<number> {
    return [...Array(this.pagesTotal).keys()];
  }
}
