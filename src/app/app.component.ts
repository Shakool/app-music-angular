import { Component } from '@angular/core';
import { interval, Observable, fromEvent } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

interface Timer {
  hour: string;
  min: string;
  second: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app-music';

  timer: Timer;
  //timerOn: boolean = true;

  constructor() {
    // On peut souscrire à un observable automatiquement à partir d'html avec "async" ( {{ timer | async |  json }} ). Il faut que le getter soit défini et nommé correctement.
    this.getTimer().subscribe((timer) => {
      this.timer = timer;
    });
  }

  getTimer(): Observable<Timer> {
    // Observable de type number
    const count$ = interval(1000);
    // const stopButton$ = fromEvent(
    //   document.querySelector('#timer-stop'),
    //   'click'
    // );

    // Si on applique un pipe sur l'observable, c'est encore un observable par contre le type est différent.
    return count$.pipe(
      map((count) => {
        return {
          hour: String(Math.floor(count / 3600) % 3600).padStart(2, '0'),
          min: String(Math.floor(count / 60) % 60).padStart(2, '0'),
          second: String(count % 60).padStart(2, '0'),
        };
      })
      //takeUntil(stopButton$)
    );
  }

  onTimerClick() {
    // if (this.timerOn) {
    //   this.timer;
    // } else {
    //   // start
    // }
    // this.timerOn = !this.timerOn;
  }
}

// Old :
// export class AppComponent {
//   title = 'app-music';
//   count: String;

//   constructor() {
//     const count$ = interval(1000); // toutes les secondes n+1
//     const observer$ = {
//       next: (count) => {
//         this.count = this.formatTime(count);
//       },
//       error: (err) => {
//         console.error('Observer got an error: ' + err);
//       },
//       complete: () => {
//         console.log('Observer got a complete notification');
//       },
//     };
//     count$.subscribe(observer$);
//   }

//   formatTime(c: number): String {
//     let formattedTime: String;

//     let h = Math.floor(c / 3600)
//       .toString()
//       .padStart(2, '0');
//     let m =
//       Math.floor(c / 60) < 60
//         ? Math.floor(c / 60)
//             .toString()
//             .padStart(2, '0')
//         : (Math.floor(c / 60) % 60).toString().padStart(2, '0');
//     let s = (c % 60).toString().padStart(2, '0');

//     // let hour = String(Math.floor( c / 3600) % 3600).padStart(2,'0');
//     // let min = String(Math.floor(c / 60 ) % 60).padStart(2, '0');
//     // let second = String(c / 60).padStart(2, '0');

//     formattedTime = `${h}:${m}:${s}`;

//     return formattedTime;
//   }
// }
