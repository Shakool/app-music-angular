import { Component, OnInit } from '@angular/core';
import { Album } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  albums: Album[];
  constructor(private AService: AlbumService) {}

  ngOnInit(): void {}

  getAverage(notes: number[]): number {
    let average: number = notes.reduce(
      (previousValue: number, currentValue: number) => {
        return previousValue + currentValue;
      }
    );
    return average / notes.length;
  }

  getPosition(id: string): number {
    // Plus simple mais peut poser un probleme de position en cas d'égalité de moyenne ( solution lazy : classer par position et pas pas moyenne pour avoir un résultat visuellement consistant ):
    // return (
    //   this.getSortedAlbumsByAverage().findIndex(item => {
    //     return item.id == id;
    //   }) + 1
    // );

    // Solution en utilisant Set :
    // On défini un tableau des moyennes uniques
    let distinctAvg = [
      ...new Set( // On a uniquement les données uniques
        this.getSortedAlbumsByAverage().map(album =>
          this.getAverage(album.notes)
        )
      )
    ];

    // On récupère la moyenne en cours
    let avg = this.getAverage(
      this.getSortedAlbumsByAverage().find(album => album.id === id).notes
    );

    // On retourne l'index de la moyenne de l'élément dans notre tableau distinct et on aura donc les égalités avec la même position.
    return distinctAvg.indexOf(avg) + 1;
  }

  // Si deux albums ont la même moyenne : il se peut que la position ne soit pas correcte au niveau de l'affichage. On peu utiliser les  3 methodes ci dessous pour faire en sorte que si égalité, les positions soient égales. ( Attention fonction récursive )
  // totoAverage(index: number, els: Album[]): number {
  //   const currentAvg = this.getTotoAverage(els[index]);
  //   if (index === 0) return index;
  //   const prevAvg = this.getTotoAverage(els[index - 1]);
  //   if (currentAvg === prevAvg) return this.totoAverage(index - 1, els);
  //   return index;
  // }

  // getTotoAverage(album: Album): number {
  //   return this.getAverage(album.notes);
  // }

  // getTotoPosition(id: string) {
  //   const albums = this.getSortedAlbumsByAverage();
  //   const currentIndex = albums.findIndex(a => a.id === id);
  //   return this.totoAverage(currentIndex, albums) + 1;
  // }
  // End

  getSortedAlbumsByAverage(sort: string = 'asc'): Album[] {
    if (sort == 'desc') {
      return this.AService.getAlbums((a, b) => {
        return this.getAverage(a.notes) - this.getAverage(b.notes);
      });
    } else {
      return this.AService.getAlbums((a, b) => {
        return this.getAverage(b.notes) - this.getAverage(a.notes);
      });
    }
  }

  setOrder(sort: string = 'asc'): void {
    // Set the order in the service ( so paginate will have our correct order )
    this.AService.order =
      sort == 'desc'
        ? (a, b) => this.getAverage(a.notes) - this.getAverage(b.notes)
        : (a, b) => this.getAverage(b.notes) - this.getAverage(a.notes);
    // Set the display for this page ( so the view refresh and we don't have the full list )
    this.albums = this.getSortedAlbumsByAverage(sort).slice(0, 4);
    // Set the current page to one ( since we override the display )
    this.AService.currentPage(1);
  }

  pageChangeParent($e): void {
    this.albums = $e;
  }
}
