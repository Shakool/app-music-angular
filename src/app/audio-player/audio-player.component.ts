import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../album.service';
import { Status, Album } from '../album';
import { interval } from 'rxjs';
import { take, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss'],
})
export class AudioPlayerComponent implements OnInit {
  showplayer: boolean = false;
  album: Album;
  currentProgress: number = 0;

  constructor(private AService: AlbumService) {}

  ngOnInit(): void {
    this.AService.subjectAlbum.subscribe((album) => {
      console.log(album.status);
      this.album = album;
      this.showplayer = album.status == Status.On ? true : false;
    });
    this.AService.subjectAlbum
      .pipe(
        switchMap((album) => {
          return interval(1).pipe(
            map((currentTime) => {
              return (currentTime / this.album.duration) * 100;
            }),
            take(album.duration)
          );
        })
      )
      .subscribe((currentProgress) => {
        console.log(currentProgress);
        this.currentProgress = currentProgress;
        if (Math.ceil(currentProgress) === 100) {
          this.reset();
        }
      });
  }

  reset() {
    this.showplayer = false;
    this.currentProgress = 0;
  }
}
