import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Album } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-album-description',
  templateUrl: './album-description.component.html',
  styleUrls: ['./album-description.component.scss']
})
export class AlbumDescriptionComponent implements OnInit {
  album: Album;

  constructor(
    private route: ActivatedRoute, // Recup le service Route
    private AService: AlbumService // Récup le service
  ) {}

  ngOnInit(): void {
    // Récup l'id;
    const id = this.route.snapshot.paramMap.get('id');
    this.album = this.AService.getAlbum(id);
  }
}
