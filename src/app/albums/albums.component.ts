import { Component, OnInit } from '@angular/core';

// Import définition de la classe et albums
import { Album, Status } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss'],
})
export class AlbumsComponent implements OnInit {
  // titlePage: string = 'Page principale Albums Music';
  albums: Album[];
  selectedAlbum: Album;
  count: number = 0;

  constructor(private AService: AlbumService) {
    // Si public, accessible via 'this.AService' dans la vue ou ici. Si private accessible dans le component uniquement.
    this.AService.order = (a, b) => b.duration - a.duration;
  }

  ngOnInit(): void {
    // With custom order :
    // this.albums = this.AService.getAlbums(this.order); // getAll
    // this.albums = this.AService.paginate(0, this.AService.getAlbumCount()); // On page with all
    // this.albums = // With Paginate component
  }

  ngOnChanges(): void {}

  showHeart(like: string): number[] {
    let number: number;
    number = like == 'Much' ? 3 : like == 'Rather' ? 2 : 1;

    // ... spread operator JS poour décompacter un itérable
    //  si on le fait dans un array JS il crééra un tableau JS
    return [...Array(number).keys()];
  }

  onClick(num: number): void {
    this.count += num;
  }

  onSelect(album: Album): void {
    // console.log(album);
    this.selectedAlbum = { ...this.AService.getAlbum(album.id) }; // Utilisation du spread pour créer un nouvel objet : du coup les détails se raffraichissent même si l'album est le même.
  }

  getDuration(sec: number): number {
    return Math.floor((10 * (sec / 60)) / 10);
  }

  playParent($e: Album): void {
    this.albums = this.albums.map((album) => {
      album.status = album.id == $e.id ? Status.On : Status.Off;
      return album;
    });
    this.AService.switchOn($e);
  }

  hideParent($e: Album): void {
    this.albums = this.albums.map((album) => {
      album.status = Status.Off;
      return album;
    });
    this.AService.switchOff($e);
  }

  searchParent($e: Album[]): void {
    this.albums = $e;
  }

  pageChangeParent($e) {
    this.albums = $e;
  }
}
