import { Injectable } from '@angular/core';
import { ALBUMS, ALBUM_LISTS } from './mock-albums';
import { Album, List, Order, Status } from './album';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AlbumService {
  private _albums: Album[] = ALBUMS;
  private _albumList: List[] = ALBUM_LISTS;
  public order: Order = (a, b) => a.duration - b.duration;
  // subject pour la pagination informer les autres components
  public sendCurrentPage = new Subject<number>();
  public subjectAlbum = new Subject<Album>();

  constructor() {}

  getAlbums(order: Order = (a, b) => a.duration - b.duration): Album[] {
    return this._albums.sort(order);
  }

  getAlbum(id: string): Album {
    return this._albums.find((album) => {
      return album.id === id;
    });
  }

  switchOn(album: Album) {
    album.status = Status.On;
    return this.subjectAlbum.next(album);
  }
  switchOff(album: Album) {
    album.status = Status.Off;
    return this.subjectAlbum.next(album);
  }

  // setAlbums(albums: Album[]): void {
  //   this._albums = albums;
  // }

  shuffleAlbumList(id: string): List {
    let list = this.getAlbumList(id);
    list.list = list.list.sort((a, b) => Math.random() - 0.5); // Randomization d'un array

    return list;
  }

  getAlbumList(id: string): List {
    let list: List = this._albumList.find((list) => {
      return list.id === id;
    });
    return list;
  }

  getAlbumCount(): number {
    return this._albums.length;
  }

  search(query: string): Album[] {
    console.log(`From Service : ${query}`);
    //if (query.length > 0) {
    return this.getAlbums(this.order).filter((album) => {
      // console.log(album.title, query, album.title.includes(query));
      return album.title.includes(query);
    });
    //}
  }

  paginate(start: number, end: number): Album[] {
    return this.getAlbums(this.order).slice(start, end);
  }

  // A appeler lors du changement de page avec le nouveau numéro
  currentPage(page: number) {
    return this.sendCurrentPage.next(page);
  }
}
